

import matplotlib.pyplot as plt
fig, ax = plt.subplots()
dias = ['L', 'M', 'X', 'J', 'V', 'S', 'D']
temperaturas = {'Cordoba':[28.5, 30.5, 31, 30, 28, 27.5, 30.5], 'Orizaba':[24.5, 25.5, 26.5, 25, 26.5, 24.5, 25]}
ax.plot(dias, temperaturas['Cordoba'],label='Cordoba')
ax.plot(dias, temperaturas['Orizaba'],label='Orizaba')
ax.set_xlabel("Días", fontdict = {'fontsize':14, 'fontweight':'bold', 'color':'tab:blue'})
ax.set_ylabel("Temperatura ºC")
ax.legend(loc='upper center')
ax.set_ylim([20,35])
ax.set_yticks(range(20, 35))
plt.show()